package com.example.phat.thread;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private TextView tvRunningNumber;
    private Button btnStart;
    public boolean stop =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar= (ProgressBar) findViewById(R.id.pbPrgressBar);
        tvRunningNumber = (TextView)findViewById(R.id.tvRunningNumber);
        btnStart = (Button)findViewById(R.id.btnStart);
    }

    Thread t = null;
    MyThread myThread = new MyThread();
    public synchronized void onBtnStartClick(View view) throws InterruptedException {
        MyAsync a = new MyAsync();
        a.execute(1);
//        if(t == null){
//            Log.i("LTP", "onBtnStartClick: start thread...");
//            t = new Thread(myThread,"MyThread");
//            t.start();
//        }
//        else{
//            Log.i("LTP", "onBtnStartClick: stopping thread...");
//            stop =!stop;
//            if(!stop){
//                Log.i("LTP", "onBtnStartClick: notify");
//                synchronized (t){
//                    t.notify();
//                }
//            }
//        }
    }

    public class MyAsync extends AsyncTask<Integer,Integer,String>{

        @Override
        protected String doInBackground(Integer... integers) {

            for (int i = 0; i <= 100; i++) {
                publishProgress(i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "Congratulations! Count is Finished!";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            tvRunningNumber.setText(String.valueOf(values[0]));
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            super.onPostExecute(s);
        }
    }

    public class MyThread implements Runnable{

        @Override
        public void run() {

            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                final int finalI = i;
                Log.i("LTP", "run: "+i);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(stop){
                    try {
                        synchronized (t){
                            t.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                        @Override
                        public void run() {tvRunningNumber.setText(String.valueOf(finalI));

                        }
                    });
            }
        }
    }
}
